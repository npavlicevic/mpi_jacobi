#ifndef JACOBI_H
#define JACOBI_H
#include <stdio.h>
#include <mpi.h>
#include "mpi_vector.h"
void mpi_jacobi(LOCAL_MATRIX_T local_m, float local_v[], float result_v[], int iterations, int n, int n_bar, int my_rank);
#endif
