#ifndef JACOBI_SERIAL_H
#define JACOBI_SERIAL_H
#include "mpi_vector.h"
void serial_read_vector(float local_v[], int n);
void serial_print_vector(float local_v[], int n);
void serial_read_matrix(LOCAL_MATRIX_T local_m, int n);
void serial_print_matrix(LOCAL_MATRIX_T local_m, int n);
void serial_jacobi(LOCAL_MATRIX_T local_m, float local_v[], float result_v[], int n, int iterations);
#endif
