#include "jacobi.h"
main(int argc, char **argv) {
  int p;
  int my_rank;
  int n;
  int n_bar;
  int iterations;
  float result_v[MAX_LOCAL_ORDER];
  float local_v[MAX_LOCAL_ORDER];
  LOCAL_MATRIX_T local_m;
  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &p);
  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
  fscanf(stdin, "%d", &n);
  MPI_Bcast(&n, 1, MPI_INT, 0, MPI_COMM_WORLD);
  fscanf(stdin, "%d", &iterations);
  MPI_Bcast(&iterations, 1, MPI_INT, 0, MPI_COMM_WORLD);
  n_bar = n / p;
  mpi_read_vector_scatter(result_v, n_bar, p, my_rank);
  mpi_print_vector(result_v, n_bar, p, my_rank);
  mpi_read_vector_scatter(local_v, n_bar, p, my_rank);
  mpi_print_vector(local_v, n_bar, p, my_rank);
  mpi_read_matrix_scatter(local_m, n_bar, n, p, my_rank);
  mpi_print_matrix(local_m, n_bar, n, p, my_rank);
  mpi_jacobi(local_m, local_v, result_v, iterations, n, n_bar, my_rank);
  mpi_print_vector(local_v, n_bar, p, my_rank);
  MPI_Finalize();
}
