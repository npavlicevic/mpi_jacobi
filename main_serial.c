#include "jacobi_serial.h"
main(int argc, char **argv) {
  int p;
  int my_rank;
  int n;
  int iterations;
  float local_v[MAX_LOCAL_ORDER];
  float result_v[MAX_LOCAL_ORDER];
  LOCAL_MATRIX_T local_m;
  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &p);
  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
  fscanf(stdin, "%d", &n);
  fscanf(stdin, "%d", &iterations);
  serial_read_vector(result_v, n);
  serial_print_vector(result_v, n);
  serial_read_matrix(local_m, n);
  serial_print_matrix(local_m, n);
  serial_jacobi(local_m, local_v, result_v, n, iterations);
  serial_print_vector(local_v, n);
  MPI_Finalize();
}
