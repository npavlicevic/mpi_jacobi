#
# Makefile 
# jacobi method for solving Ax=b
#

CC=mpicc
LIBS=-lmpi -lmpivector
FILES=jacobi.c main.c
FILES_SERIAL=jacobi_serial.c main_serial.c
REMOVE=main main_serial

all: main serial

main: ${FILES}
	${CC} $^ -o main ${LIBS}

serial: ${FILES_SERIAL}
	${CC} $^ -o main_serial ${LIBS}

clean: ${REMOVE}
	rm $^
