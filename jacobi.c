#include "jacobi.h"
void mpi_jacobi(LOCAL_MATRIX_T local_m, float local_v[], float result_v[], int iterations, int n, int n_bar, int my_rank) {
  float _local_v[MAX_LOCAL_ORDER];
  MPI_Allgather(local_v, n_bar, MPI_FLOAT, _local_v, n_bar, MPI_FLOAT, MPI_COMM_WORLD);
  int i, j, k, iteration = 0;
  do {
    iteration++;
    for(i = 0, j = my_rank * n_bar; i < n_bar; i++, j++) {
      // local_v each part starts from 0
      // result_v each part starts from 0
      printf("(%d %d) ", i, j);
      local_v[i] = result_v[i];
      for(k = 0; k < j; k++) {
        local_v[i] -= local_m[i][k] * _local_v[j];
      }
      for(k = j + 1; k < n; k++) {
        local_v[i] -= local_m[i][k] * _local_v[j];
      }
      local_v[i] /= local_m[i][j];
    }
    printf("\n");
    MPI_Allgather(local_v, n_bar, MPI_FLOAT, _local_v, n_bar, MPI_FLOAT, MPI_COMM_WORLD);
    // need all local_v
  } while(iteration < iterations);
}
