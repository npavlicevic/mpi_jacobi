#include "jacobi_serial.h"
void serial_read_vector(float local_v[], int n) {
  int i = 0;
  for(; i < n; i++) {
    fscanf(stdin, "%f", &local_v[i]);
  }
}
void serial_print_vector(float local_v[], int n) {
  int i = 0;
  for(; i < n; i++) {
    printf("%f ", local_v[i]);
  }
  printf("\n");
}
void serial_read_matrix(LOCAL_MATRIX_T local_m, int n) {
  int i = 0, j;
  for(; i < n; i++) {
    for(j = 0; j < n; j++) {
      fscanf(stdin, "%f", &local_m[i][j]);
    }
  }
}
void serial_print_matrix(LOCAL_MATRIX_T local_m, int n) {
  int i = 0, j;
  for(; i < n; i++) {
    for(j = 0; j < n; j++) {
      printf("%f ", local_m[i][j]);
    }
    printf("\n");
  }
}
void serial_jacobi(LOCAL_MATRIX_T local_m, float local_v[], float result_v[], int n, int iterations) {
  int i, j, iteration = 0;
  float _local_v[MAX_LOCAL_ORDER];
  for(i = 0; i < n; i++) {
    local_v[i] = result_v[i];
  }
  do {
    iteration++;
    for(i = 0; i < n; i++) {
      _local_v[i] = local_v[i];
    }
    for(i = 0; i < n; i++) {
      local_v[i] = result_v[i];
      for(j = 0; j < i; j++) {
        local_v[i] -= local_m[i][j] * _local_v[i];
      }
      for(j = i + 1; j < n; j++) {
        local_v[i] -= local_m[i][j] * _local_v[i];
      }
      local_v[i] /= local_m[i][i];
    }
  } while(iteration < iterations);
}
